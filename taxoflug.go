package main

import (
    "net/http"
    "time"
    "encoding/json"
    "fmt"
    "sync"
    "os"
    "os/exec"
    "errors"
    "path"
    "flag"
    "io/ioutil"
    log "github.com/Sirupsen/logrus"
    lumberjack "gopkg.in/natefinch/lumberjack.v2"
    "bufio"
    "regexp"
    "strconv"
    "sort"
)

type FlightInfo struct {
    Airport string
    Arrival time.Time
    IsCanceled bool
}

type TimetableCache struct {
    PluginName string
    Priority int
    Airports []string
    ByFlightCode map[string][]FlightInfo
    SyncErrorCount uint64
}

type ServerState struct {
    Caches               []TimetableCache
    CachesMutex          sync.Mutex

    MaxSyncErrorCount    uint64
    DefaultBeginTsOffset int64
}

// Items in this array are sorted by priority
var appState ServerState

func LookupFlightInfoList(flightCode string) ([]FlightInfo, bool) {
    appState.CachesMutex.Lock()
    hasValidCaches := false
    for _, cache := range appState.Caches {
        // Skip inferior caches
        if cache.SyncErrorCount > appState.MaxSyncErrorCount {
            continue
        }

        hasValidCaches = true
        flightInfos, ok := cache.ByFlightCode[flightCode]

        if ok {
            appState.CachesMutex.Unlock()
            return flightInfos, hasValidCaches
        }
    }

    appState.CachesMutex.Unlock()
    return nil, hasValidCaches
}

func FlightGetHandler(w http.ResponseWriter, r *http.Request) {
    requestId := r.URL.Query().Get("id")

    flightCode := r.URL.Query().Get("flight_no")
    if len(flightCode) == 0 {
        http.Error(w, "Missing parameter flight_no.", 400)
        return
    }

    timestampLowerBoundStr := r.URL.Query().Get("min_ts")
    timestampLowerBound := time.Now().UTC().Unix() + appState.DefaultBeginTsOffset
    if len(timestampLowerBoundStr) != 0 {
        var err error
        timestampLowerBound, err = strconv.ParseInt(timestampLowerBoundStr, 10, 64)
        if err != nil {
            http.Error(w, fmt.Sprintf("Failed to parse parameter min_ts: %s", timestampLowerBoundStr), 400)
            return
        }
    }

    type Response struct {
        RequestId   string `json:"id,omitempty"`
        FlightCode  string `json:"flight_no"`
        Airport     string `json:"airport"`
        ArrivalDate string `json:"bdate"`
        ArrivalTime string `json:"btime"`
        Timestamp   int64  `json:"timestamp"`
        Canceled    bool   `json:"canceled"`
    }

    flightInfoList, hasValidCaches := LookupFlightInfoList(FlightNumberStripZeroes(flightCode))
    if flightInfoList != nil {
        // Find earliest flight that lands at timestampLowerBound or later.
        upcomingFlightIndex := -1
        for i, flightInfo := range flightInfoList {
            ts := flightInfo.Arrival.Unix()
            if ts >= timestampLowerBound && (upcomingFlightIndex == -1 || ts < flightInfoList[upcomingFlightIndex].Arrival.Unix()) {
                upcomingFlightIndex = i
            }
        }

        if upcomingFlightIndex != -1 {
            upcomingFlightInfo := flightInfoList[upcomingFlightIndex]
            responseObj := Response{
                RequestId: requestId,
                FlightCode: flightCode,
                Airport: upcomingFlightInfo.Airport,
                ArrivalDate: upcomingFlightInfo.Arrival.Format("02.01.2006"),
                ArrivalTime: upcomingFlightInfo.Arrival.Format("15:04"),
                Timestamp: upcomingFlightInfo.Arrival.Unix(),
                Canceled: upcomingFlightInfo.IsCanceled,
            }
            responseData, _ := json.Marshal(responseObj)
            fmt.Fprint(w, string(responseData))
        } else {
            http.Error(w, "All matching flights land earlier than the requested lower bound.", 404)
        }
    } else {
        if hasValidCaches {
            http.Error(w, "Flight not found.", 404)
        } else {
            http.Error(w, "No recent data available.", 500)
        }
    }
}

type PluginOutputTimetableItem struct {
    Number string `json:"number"`
    Status string `json:"status"`
    Timestamp int64 `json:"timestamp"`
}

type PluginOutputAirportTimetable struct {
    Error string `json:"error"`
    Arrivals []PluginOutputTimetableItem `json:"arrivals"`
}

type PluginOutputTimetable map[string]PluginOutputAirportTimetable

func RunPyPlugin(pluginName string, path string, confJson string) (*PluginOutputTimetable, error) {
    cmd := exec.Command("/usr/bin/python3", path, confJson)

    stdout, err := cmd.StdoutPipe()
    if err != nil {
        return nil, err
    }

    stderr, err := cmd.StderrPipe()
    if err != nil {
        return nil, err
    }

    if err = cmd.Start(); err != nil {
        return nil, err
    }

    go func() {
        stderrReader := bufio.NewReader(stderr)
        for {
            line, _, _ := stderrReader.ReadLine()
            if line == nil {
                break
            }

            log.WithField("plugin", pluginName).Error(string(line))
        }
    }()

    var pyPluginOutput struct {
        Error *string `json:"error"`
        Result *PluginOutputTimetable `json:"result"`
    }
    if err := json.NewDecoder(stdout).Decode(&pyPluginOutput); err != nil {
        return nil, err
    }

    if err := cmd.Wait(); err != nil {
        return nil, err
    }

    if pyPluginOutput.Error != nil {
        return nil, errors.New("Plugin error: " + *pyPluginOutput.Error)
    } else if pyPluginOutput.Result == nil {
        return nil, errors.New("Unknown plugin error.")
    } else {
        return pyPluginOutput.Result, nil
    }
}

func FlightNumberStripZeroes(flight string) string {
    // Remove all zeroes following a space, like in "SU 004"
    r := regexp.MustCompile(" 0+")
    return r.ReplaceAllString(flight, " ")
}

func FindCacheIndex(pluginName string) int {
    index := int(-1)
    for i, cache := range appState.Caches {
        if cache.PluginName == pluginName {
            index = i
            break
        }
    }

    return index
}

func RunScraper(pluginsPath string, pluginName string, airports []string) {
    type PluginConf struct {
        Airports []string `json:"airports"`
    }

    confObj := PluginConf{Airports: airports}
    confData, _ := json.Marshal(confObj)

    result, err := RunPyPlugin(pluginName, path.Join(pluginsPath, pluginName + ".py"), string(confData))
    appState.CachesMutex.Lock()
    index := FindCacheIndex(pluginName)
    if err != nil {
        log.WithFields(log.Fields{
            "plugin": pluginName,
            "error": err.Error(),
        }).Error("Failed to run plugin")

        appState.Caches[index].SyncErrorCount++
    } else {
        downloadedCount := 0
        if index != -1 {
            byFlightCode := map[string][]FlightInfo{}
            for airportCode, airportTimetable := range *result {
                if len(airportTimetable.Error) == 0 {
                    for _, item := range airportTimetable.Arrivals {
                        flightNumber := FlightNumberStripZeroes(item.Number)

                        flightInfo := FlightInfo{
                            Airport: airportCode,
                            Arrival: time.Unix(item.Timestamp, 0).UTC(),
                            IsCanceled: item.Status == "canceled",
                        }

                        _, present := byFlightCode[flightNumber]
                        if !present {
                            byFlightCode[flightNumber] = make([]FlightInfo, 0)
                        }

                        byFlightCode[flightNumber] = append(byFlightCode[flightNumber], flightInfo)
                        downloadedCount++
                    }
                }
            }

            appState.Caches[index].ByFlightCode = byFlightCode
            appState.Caches[index].SyncErrorCount = 0
        }

        log.WithField("downloadedCount", downloadedCount).Info(fmt.Sprintf("Call to plugin finished: %s", pluginName))
    }
    appState.CachesMutex.Unlock()
}

func RunScraperLoop(downloadInterval int, pluginsPath string, pluginName string, airports []string) {
    for {
        RunScraper(pluginsPath, pluginName, airports)
        time.Sleep(time.Duration(downloadInterval) * time.Second)
    }
}

// Interface to sort plugins by priority.
type PluginPrioritySorter []TimetableCache
func (a PluginPrioritySorter) Len() int           { return len(a) }
func (a PluginPrioritySorter) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a PluginPrioritySorter) Less(i, j int) bool { return a[i].Priority < a[j].Priority }

func main() {
    var configFilePath string
    flag.StringVar(&configFilePath, "config-file", "/etc/taxoflug.conf", "Path to configuration file in JSON format")
    flag.Parse()

    configFileContent, err := ioutil.ReadFile(configFilePath)
    if err != nil {
        fmt.Printf("File error: %v\n", err)
        os.Exit(1)
    }

    type PluginConfig struct {
        Name *string
        Priority *int
        Airports *[]string
    }

    type ConfigFile struct {
        PluginsPath          *string
        Plugins              *[]PluginConfig

        ListenUrl            *string

        LogFilePath          *string
        LogMaxSize           *int
        LogMaxBackups        *int
        LogMaxAge            *int

        DownloadInterval     *int

        MaxSyncErrorCount    *uint64
        DefaultBeginTsOffset *int64
    }

    configObj := ConfigFile{}
    err = json.Unmarshal(configFileContent, &configObj)
    if err != nil {
        fmt.Printf("Failed to parse JSON from config file %s: %s\n", configFilePath, err)
        fmt.Printf("Content: %s\n", configFileContent)
        os.Exit(2)
    }

    if configObj.PluginsPath == nil || configObj.Plugins == nil ||
            configObj.ListenUrl == nil ||
            configObj.LogFilePath == nil || configObj.LogMaxSize == nil ||
            configObj.LogMaxBackups == nil || configObj.LogMaxAge == nil ||
            configObj.DownloadInterval == nil || configObj.MaxSyncErrorCount == nil ||
            configObj.DefaultBeginTsOffset == nil {
        fmt.Printf("Config file %s is missing a required field\n", configFilePath)
        os.Exit(3)
    }

    log.SetOutput(&lumberjack.Logger{
        Filename: *configObj.LogFilePath,
        MaxSize: *configObj.LogMaxSize, // file max size in megabytes
        MaxBackups: *configObj.LogMaxBackups, // number of files
        MaxAge: *configObj.LogMaxAge, // file max age in days
    })

    customFormatter := new(log.TextFormatter)
    customFormatter.FullTimestamp = true
    customFormatter.TimestampFormat = time.RFC3339Nano
    log.SetFormatter(customFormatter)

    cachesList := make([]TimetableCache, 0)
    for index, plugin := range *configObj.Plugins {
        if plugin.Name == nil || plugin.Priority == nil || plugin.Airports == nil {
            fmt.Printf("Warning: Configuration of plugin #%s is missing a required field.\n", index)
        } else {
            cachesList = append(cachesList, TimetableCache{
                PluginName: *plugin.Name,
                Priority: *plugin.Priority,
                Airports: *plugin.Airports,
                ByFlightCode: map[string][]FlightInfo{},
                SyncErrorCount: appState.MaxSyncErrorCount + 1,
            })
        }
    }

    // Sort the plugins by priority.
    sort.Sort(PluginPrioritySorter(cachesList))

    appState = ServerState{
        Caches: cachesList,
        MaxSyncErrorCount: *configObj.MaxSyncErrorCount,
        DefaultBeginTsOffset: *configObj.DefaultBeginTsOffset,
    }

    for _, plugin := range appState.Caches {
        go RunScraperLoop(*configObj.DownloadInterval, *configObj.PluginsPath, plugin.PluginName, plugin.Airports)
    }

    log.Info(fmt.Sprintf("Starting HTTP server at %s.", *configObj.ListenUrl))

    http.HandleFunc("/method/flight.get", FlightGetHandler)
    http.ListenAndServe(*configObj.ListenUrl, nil)
}
