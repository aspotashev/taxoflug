import pytest

from tests.fixture.server import ServerFixture


@pytest.fixture(scope="module")
def server():
    fixture = ServerFixture()
    yield fixture
    fixture.teardown()
