#!/usr/bin/python3

import json
import sys


def download_airport_timetable():
    res_arrivals = [
        {
            'number': 'SU 012',
            'status': 'landed',
            'timestamp': 1400000000,
        },
        {
            'number': 'SU 012',
            'status': 'estimated',
            'timestamp': 1400000123,
        },
        {
            'number': 'ВЖЖЖ 1234',
            'status': 'estimated',
            'timestamp': 1400000123,
        },
    ]

    return {'arrivals': res_arrivals}


def download_airport_list(iata_codes):
    res = {}
    for iata_code in iata_codes:
        if iata_code == 'LED':
            res[iata_code] = download_airport_timetable()
        else:
            res[iata_code] = {
                'error': 'Airport %s not supported.' % iata_code
            }

    return res


if len(sys.argv) != 2:
    raise RuntimeError("Expected exactly one command line argument")

airports = json.loads(sys.argv[1])['airports']

download_airport_timetable()
result = download_airport_list(airports)
output_obj = {'result': result}
print(json.dumps(output_obj))
