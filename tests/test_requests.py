def test_not_found(server):
    response = server.request({
        'flight_no': 'SU 321',
    })
    assert response.status_code == 404
    assert response.content == b'Flight not found.\n'


def test_request(server):
    response = server.request({
        'flight_no': 'SU 012',
        'min_ts': 1300000000,
    })
    assert response.status_code == 200
    assert response.content == (
        b'{"flight_no":"SU 012","airport":"LED","bdate":"13.05.2014","btime":"16:53",'
        b'"timestamp":1400000000,"canceled":false}')
