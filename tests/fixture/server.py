import json
import os
import signal
import tempfile
import threading
import time
from subprocess import Popen, PIPE

import requests


class ServerFixture(object):
    def __init__(self):
        self.exec_file = self.create_exec_file()
        self.log_file = self.create_log_file()
        self.config_file = self.create_config_file(self.log_file)

        self.taxoflug_proc = None
        self.taxoflug_thread = None
        self.start_taxoflug()

    @staticmethod
    def create_exec_file():
        f = tempfile.NamedTemporaryFile(delete=False, prefix='taxoflug-exec-')
        res = f.name
        f.close()
        return res

    @staticmethod
    def create_log_file():
        f = tempfile.NamedTemporaryFile(delete=False, prefix='taxoflug-log-')
        res = f.name
        f.close()
        return res

    @staticmethod
    def create_config_file(log_file):
        plugins_path = os.path.join(os.path.dirname(__file__), '../plugin_mocks')

        config = {
            "listenUrl": ":9123",
            "logFilePath": log_file,
            "logMaxSize": 1000,
            "logMaxBackups": 3,
            "logMaxAge": 28,
            "pluginsPath": plugins_path,
            "plugins": [
                {
                    "name": "pluginmock1",
                    "priority": 100,
                    "airports": ["LED", "OMS", "LTK", "VKO"]
                },
                {
                    "name": "pulkovoairport",
                    "priority": 1,
                    "airports": ["LED", "VKO"]
                }
            ],
            "downloadInterval": 60,
            "maxSyncErrorCount": 3,
            "defaultBeginTsOffset": -7200
        }

        f = tempfile.NamedTemporaryFile(delete=False)
        config_file = f.name
        f.write(json.dumps(config).encode(encoding='UTF-8') + b"\n")
        f.close()

        return config_file

    def start_taxoflug(self):
        go_file = os.path.join(os.path.dirname(__file__), '../../taxoflug.go')
        go_build_proc = Popen(
            ['go', 'build', '-o', self.exec_file, go_file],
            stdout=PIPE)
        output = go_build_proc.stdout.read()
        if len(output) > 0:
            print("Go build output: " + output)

        self.taxoflug_thread = threading.Thread(target=self.start_taxoflug_func)
        self.taxoflug_thread.start()
        time.sleep(0.5)

    def start_taxoflug_func(self):
        self.taxoflug_proc = Popen(
            [self.exec_file, '--config-file', self.config_file],
            stdout=PIPE)

        output = self.taxoflug_proc.stdout.read()
        if len(output) > 0:
            print("Server output: " + output.decode('UTF-8'))

        self.taxoflug_proc.wait()

    def teardown(self):
        os.kill(self.taxoflug_proc.pid, signal.SIGTERM)
        self.taxoflug_thread.join()
        os.unlink(self.exec_file)
        os.unlink(self.config_file)
        os.unlink(self.log_file)

    def request(self, params):
        return requests.get('http://localhost:9123/method/flight.get', params)
