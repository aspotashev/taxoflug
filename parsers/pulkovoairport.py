#!/usr/bin/python3

import datetime
import sys
import time
import json

import requests

import common


def download_airport_timetable():
    datetime_format = '%Y-%m-%d %H:%M:%S'
    res_arrivals = []
    for i in range(5):
        url = "https://www.pulkovoairport.ru/f/flights/cur/en_arr_%d.js" % i
        response = requests.get(url)
        if response.status_code != 200:
            msg = 'request %s failed with HTTP status %d' % (url, response.status_code)
            return {'error': msg}

        obj = json.loads(response.content.decode('utf8'))
        flights = obj['data']

        server_time_now_str = obj['now']
        # Example format: '2016-12-26 01:26:01'
        server_time_now = time.strptime(server_time_now_str, datetime_format)
        server_dt = datetime.datetime(*server_time_now[:6])
        utc_dt = datetime.datetime.utcnow()
        server_time_delta = server_dt - utc_dt

        total_seconds = server_time_delta.total_seconds()
        total_hours = round(total_seconds / 3600.0)
        skew_seconds = server_time_delta.total_seconds() - 3600.0 * total_hours
        if abs(skew_seconds) > 240:
            print("Warning: Server timezone is too far from 1-hour boundary: timedelta = %f, skew_seconds = %f" % (total_seconds, skew_seconds), file=sys.stderr)
            delta = datetime.timedelta(minutes=round(total_seconds / 60.0))
        else:
            delta = datetime.timedelta(hours=total_hours)

        for flight in flights:
            arrival_time_obj = flight['arrival_time']

            arrival_time_str = None
            if 'actual' in arrival_time_obj:
                arrival_time_str = arrival_time_obj['actual']
            elif 'estimated' in arrival_time_obj:
                arrival_time_str = arrival_time_obj['estimated']
            elif 'scheduled' in arrival_time_obj:
                arrival_time_str = arrival_time_obj['scheduled']

            tzinfo = datetime.timezone(delta, 'server')
            arrival_time = datetime.datetime.strptime(arrival_time_str, datetime_format).replace(tzinfo=tzinfo)

            orig_status = flight['status']
            if orig_status == 'Arrived':
                status = 'landed'
            elif orig_status == 'Canceled':
                status = 'canceled'
            elif orig_status == 'Delayed':
                status = 'delayed'
            elif orig_status == '':
                status = 'unknown'
            else:
                status = 'unknown'
                print("Unknown flight status: %s" % orig_status, file=sys.stderr)

            res_arrivals.append({
                'number': common.filter_flight_number(flight['number']),
                'status': status,
                'timestamp': int(arrival_time.timestamp())
            })

    return {'arrivals': res_arrivals}


def download_airport_list(iata_codes):
    res = {}
    for iata_code in iata_codes:
        if iata_code == 'LED':
            res[iata_code] = download_airport_timetable()
        else:
            res[iata_code] = {
                'error': 'Airport %s not supported.' % iata_code
            }

    return res


if len(sys.argv) != 2:
    raise RuntimeError("Expected exactly one command line argument")

airports = json.loads(sys.argv[1])['airports']

download_airport_timetable()
result = download_airport_list(airports)
output_obj = {'result': result}
print(json.dumps(output_obj))
