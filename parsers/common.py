import re


def filter_flight_number(number):
    return re.sub(r'^FV ', 'SU ', number)
