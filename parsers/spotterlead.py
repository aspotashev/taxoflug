#!/usr/bin/python3

import json
import re
import sys

import requests
from bs4 import BeautifulSoup

import common


class DownloadedAirportUrls(object):
    def __init__(self):
        self.__urls = {}

    def populate_from_html(self, html_doc):
        soup = BeautifulSoup(html_doc, 'html.parser')
        # print(soup.prettify())
        a_tags = soup.select('a[href^="/airports/rossiya/"]')
        urls = list(set(a['href'] for a in a_tags))

        for url in urls:
            print(url)

    def populate_from_url(self, url):
        headers = {
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.75 Safari/537.36',
        }
        response = requests.get(url, headers=headers)
        print(response.status_code, file=sys.stderr)
        html_doc = response.content

        self.populate_from_html(html_doc)


def download_airport_timetable(url):
    m = re.search('^/(.*)/spotting', url)
    icao_code = m.group(1)

    headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.75 Safari/537.36',
    }
    params = {
        'id': icao_code,
        'stepMin': 1440
    }
    response = requests.get('https://spotterlead.ru/live/GetData', params=params, headers=headers)
    if response.status_code != 200:
        msg = 'request %s failed with HTTP status %d' % (str(params), response.status_code)
        return {'error': msg}

    response_obj = json.loads(response.content.decode('utf8'))
    if response_obj['success'] != True:
        msg = 'request %s failed' % (str(params),)
        return {'error': msg}

    data_obj = response_obj['data']
    if type(data_obj) != dict:
        print('error: data_obj is %s: %s' % (type(data_obj), str(data_obj)), file=sys.stderr)
        return None

    intervals_arr = data_obj['intervals']

    if len(intervals_arr) != 1:
        print('error', file=sys.stderr)
    interval_obj = intervals_arr[0]

    interval_arrivals_arr = interval_obj['arrivals']
    res_arrivals = []
    for arrival in interval_arrivals_arr:
        status_text = arrival['status']
        status_id = arrival['statusId']
        if status_text == 'прилетел' and status_id == 9:
            status = 'landed'
        elif status_text == 'задержан' and status_id == 7:
            status = 'delayed'
        elif status_text == 'ожидается' and status_id == 1:
            status = 'expected'
        elif status_text == 'по расписанию' and status_id == 0:
            status = 'on-time'
        elif status_text == 'на подходе' and status_id == 8:
            status = 'approaching'
        else:
            print('unexpected status_text = %s, status_id = %d' % (status_text, status_id), file=sys.stderr)
            status = 'unknown'

        res_arrivals.append({
            'number': common.filter_flight_number(arrival['number']),
            'status': status,
            'timestamp': arrival['timeUnixUtc']
        })

    return {'arrivals': res_arrivals}


def download_airport_list(iata_codes):
    headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.75 Safari/537.36',
    }
    url = 'https://spotterlead.net/ru'
    response = requests.get(url, headers=headers)
    if response.status_code != 200:
        msg = 'request %s failed with HTTP status %d' % (url, response.status_code)
        raise RuntimeError(msg)

    html_doc = response.content

    soup = BeautifulSoup(html_doc, 'html.parser')
    a_tags = soup.select('a[href$="/spotting"]')

    hrefs = {}
    for a in a_tags:
        iata_code_tag = a.select('span.iata-code')[0]
        iata_code = iata_code_tag.text
        hrefs[iata_code] = a['href']

    res = {}
    for iata_code in iata_codes:
        if iata_code in hrefs:
            res[iata_code] = download_airport_timetable(hrefs[iata_code])
        else:
            res[iata_code] = {
                'error': 'Airport not found. List of available airports: %s.' % ', '.join(sorted(hrefs.keys()))
            }

    return res


if len(sys.argv) != 2:
    raise RuntimeError("Expected exactly one command line argument")

airports = json.loads(sys.argv[1])['airports']

try:
    result = download_airport_list(airports)
    output_obj = {'result': result}
except RuntimeError as e:
    output_obj = {'error': str(e)}

print(json.dumps(output_obj))
